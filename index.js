const express = require('express');
const mongoose = require('mongoose');
// Allows us to control app's Cross Origin Resource Sharing Settings
const cors = require('cors');

const app= express();
const port = 4000;

// Routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')

// Allows all resources/origin to access our backend application
// Enable all CORS requests

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}))
// Defines the '/api/users' string to be included for all the routes defined in the'user' route file
app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)

mongoose.connect("mongodb+srv://dbUser:dbUser@wdc028-course-booking.0pav9.mongodb.net/batch144_booking_system?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
})


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))


app.listen(port, () => console.log(`Now listening to port ${port}`))