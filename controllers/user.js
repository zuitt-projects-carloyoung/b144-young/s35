const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Check if the email already exists
// 1.) use find() method to find duplicate emails
// 2.) Error Handling, if no duplicate found, return false, else, return true

// Important Note: best practice to return a result is to use a boolean or return an object/array of object because string is limited in our backend, and cant be connected to our frontend


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email }).then(result => {
		if(result.length > 0){
			return true;
		}else{
			// no duplicate email found
			return false;
		}
	})
}

// User Registration
// Steps: 
//1.) create a new user object using the mongoose model and the info from the request body
//2.) make sure that the password is encrpyted
//3.) Save the new user to the database

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrpyt the password
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((user,error) => {
		if(error){
			return false;
		}else{
			return true
		}
	})
} 

// User Authentication
// Steps:
// 1. Check if the user email exist in our database. If user does not exist, return false
// 2. if the user exists, compare the password provided in the login form with the password stored in the database.
// 3. Generate or return a jsonwebtoken if the user is successfully logged in and return false if not

module.exports.loginUser = (reqBody) => {
	// findOne() method returns the first record in the collection that matches the search criteria
	// we use findOne() instead of 'find' method which returns all records that match the search criteria
	return User.findOne({ email: reqBody.email }).then(result => {
		// user does not exist
		if(result == null){
			return false;
		}else{
			// User exists
			// the "compareSync" method is used to compare a non-encrypted password from the login form to the encrypted password retreived from the database and returns true or false value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// if the password match/result of the above code is tru
			if(isPasswordCorrect){
				// generate access token
				// the mongoose toObject method converts the mongoose object into a plain javascript object
				return{ accessToken: auth.createAccessToken(result.toObject())}
			}else{
				// password do not match
				return false
			}


		}
	})
}

// ACTIVITY
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";
		
		// Returns the user information with the password as an empty string
		return result;
	});
}









